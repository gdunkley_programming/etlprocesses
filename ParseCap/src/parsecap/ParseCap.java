/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parsecap;



import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;


/**
 *
 * @author gdunkley
 */
public class ParseCap {

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */
    
    public static void main(String[] args) throws FileNotFoundException, IOException {
        
        // declare arguments from main.
        String csvFile1 = args[0];  // csv file argument
        String txtFile1 = args[1];  // txt file argument
        String outputFile = args[2];  //output file argument
        
        // declare string arrays.
        String[] arrayFile1 = new String[2000000];
        String[] arrayFile2 = new String[2000000];
        String[] arrayFileFinal = new String[1000000];
                       
        // create new objects
        OpenReadWriteFile objOrwf1 = new OpenReadWriteFile();
        OpenReadWriteFile objOrwf2 = new OpenReadWriteFile();
        OpenReadWriteFile objOrwfFinal = new OpenReadWriteFile();
        
        
        /**
         * File 1 open, read to array and print contents.
         */
        objOrwf1.OpenFile(csvFile1);
        objOrwf1.ReadToArray(arrayFile1);
        
        /**
         * File 2 open, read to array and print contents
         */
        objOrwf2.OpenFile(txtFile1);
        objOrwf2.TextToCsvArray(arrayFile2);
        // write file to new csv text file, for future use.
        objOrwf2.WriteFile(arrayFile2, "parsedCSV_ldevBySubDec.txt");
        
        //objOrwf2.PrintArray(arrayFile2); // prints on screen the array contents
        
        /**
         *         
        System.out.println();
        System.out.println("###################List2########################");
        System.out.println();
        
        for (String strVal : arrayFile2)
        {
            if (strVal != null)
            {
                List<String> arrayList2 = Arrays.asList(strVal.split(","));
                //            System.out.println(arrayList2.toString());
                if (!"0".equals(arrayList2.get(8)) && !"0".equals(arrayList2.get(11))
                        && arrayList2.get(12) !="0" && arrayList2.get(13) !="0")
                {
                    System.out.println(arrayList2.get(1) + "\t" + arrayList2.get(5)
                            + "\t" + arrayList2.get(6) + "\t" + arrayList2.get(8)    
                            + "\t" + arrayList2.get(11) + "\t" + arrayList2.get(12)
                            + "\t" + arrayList2.get(13));
                }
            }
        }
         * 
         */
        
        System.out.println();
        System.out.println("################### New CSV Creation ########################");
        System.out.println();
        
        
        StringBuilder strBld1 = new StringBuilder(); // is this needed???
        int i=0;
        int j=0;
        
        for (String strValLine : arrayFile1)
        {
            if (strValLine != null)
            {

                
                List<String> arrayList1 = Arrays.asList(strValLine.split(","));
                String hexNum = arrayList1.get(1).replace(":", "");
                
                if (!"LDEV Number".equals(hexNum)
                        && !"0.0".equals(arrayList1.get(2))
                        && !"0.0".equals(arrayList1.get(3)))
                {
                    strBld1.append(strValLine).append(",");
                    
                    int ldev = Integer.parseInt(hexNum, 16);                    
                    int count = j % 100;                    
                    if (count == 0) {
                        System.out.print("*");                        
                    }                        
                    int count1 = j % 1000;                    
                    if (count1 == 0) {
                        System.out.println();
                        System.out.println("Line : " + j + " hit!");
                    }
                    int count2 = j % 100000;                    
                    if (count2 == 0) {
                        System.out.println();
                        System.out.println("Line : " + j + " hit!");
                    }
                    j++;
                        
                    /**
                     * New loop for second file.... 
                     */

                    for (String strValLine2 : arrayFile2)
                    {
                        if (strValLine2 != null)
                        {
                            List<String> arrayList2 = Arrays.asList(strValLine2.split(","));
                            String decNum = arrayList2.get(1);
                            
                            if (!"LDEV#".equals(decNum))
                            {
                                int ldev2 = Integer.parseInt(arrayList2.get(1));
                                if (ldev == ldev2)
                                {
                                    strBld1.append(arrayList2.get(5))
                                        .append(",")
                                        .append(arrayList2.get(6))
                                        .append(",")
                                        .append(arrayList2.get(8))
                                        .append(",")
                                        .append(arrayList2.get(11))
                                        .append(",")
                                        .append(arrayList2.get(12))
                                        .append(",")
                                        .append(arrayList2.get(13))
                                        .append(",");
                                    
                                }
                            }
                        }
                    }
                    strBld1.append("\n");
                }
            }
        }
        arrayFileFinal[i++] = strBld1.toString();    
        
        System.out.println();
//        for (String strVal : arrayFileFinal){
//            if (strVal != null)
//                System.out.println(strVal);
//        }
        
        System.out.println("Finished building array now writing file.");
        System.out.println();
        
        objOrwfFinal.WriteFile(arrayFileFinal, outputFile);
        System.out.println("Finished writing array file.");
        System.out.println();
                
        
        objOrwf1.CloseF();
        objOrwf2.CloseF();


    }
}

        

 