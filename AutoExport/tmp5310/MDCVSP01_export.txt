svpip 10.139.7.56
login HDSExp ort "hdsview"
show
group PhyPG short
group PhyLDEV short
group PhyProc short
group PhyCMPK
group PhyMPPK
group PG
group LDEV
group Port
group MFPort
group PortWWN
group LU
group PPCGWWN
group RemoteCopy
group RCLU
group UniversalReplicator
group URJNL
group URLU
group URLDEV
group LDEVEachOfCU
shortrange -2400:     
longrange -030000:
outpath MDCVSP01
option nocompress
apply
