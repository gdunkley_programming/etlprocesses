/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package autoexport;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 *
 * @author gdunkley
 */
public class ExportConfig {
    
    
    public String cliCmd(String model, File subSysJLib, File jSanRmi, String exporTxt, File logDir){
        
        String ncli = new String();
        
        switch (model) {
            case "VSP":
                System.out.println("initlizing export of HDS VSP 700 series...");
                File cliVsp = new File("java -classpath " 
                        + subSysJLib 
                        + "/JSanExport.jar:" 
                        + jSanRmi 
                        + " -Xmx536870912 -Dmd.command=" 
                        + exporTxt 
                        + " -Dmd.logpath=" 
                        + logDir 
                        + " -Dmd.rmitimeout=20 sanproject.getmondat.RJMdMain");
                        
                ncli = cliVsp.toString();
                break;
            
            case "G1000":
                System.out.println("initlizing export of HDS VSPG1000 800 series...");
                File cliG1000 = new File("java -classpath " 
                        + subSysJLib
                        + "/JSanExportLoader.jar -Del.tool.Xmx=536870912 -Dmd.command=" 
                        + exporTxt
                        + " -Del.logpath="
                        + logDir
                        + " -Dmd.rmitimeout=20 sanproject.getexptool.RJElMain");
                
                ncli = cliG1000.toString();
                break;
            
            case "Gx00":
                System.out.println("initlizing export of HDS Gx00 modular series...");
                File cliGx00 = new File("java -classpath "
                        + subSysJLib
                        + "/JSanExportLoader.jar -Del.tool.Xmx=536870912 -Dmd.command="
                        + exporTxt
                        + " -Del.logpath="
                        + logDir
                        + " -Dmd.rmitimeout=20 sanproject.getexptool.RJElMain");
                
                ncli = cliGx00.toString();
                break;
                
            default:
                System.out.println("Error - Inconpatable model type, please check config file...");
                break;
        }
        return ncli;
    }

    public void writeFile(List<String> fileVal, String fileName) throws FileNotFoundException{
        
        try {
            File file = new File(fileName);
            PrintWriter output = new PrintWriter(file);
            
            fileVal.stream().filter((strVal) -> (strVal != null)).forEach((strVal) -> {
                output.println(strVal);
            });
            System.out.println("File Created: " + fileName);
            output.close();
        }
        
        catch (Exception e) {
             System.out.println("Error - ...");
        }
        
    }
    
    public void runExe(String strCmd) throws IOException{
        
        try {
            System.out.println("starting to run...");
            Runtime runCmd = Runtime.getRuntime();
            runCmd.exec(strCmd).waitFor();
            System.out.println("executed program...");
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    

    public void delDirAll(File workDir) {
        File[] dirContents = workDir.listFiles();
        
        if (dirContents != null) {
            for (File fileVal: dirContents) {
                delDirAll(fileVal);
            }
        }
        workDir.delete();
    }
    
}