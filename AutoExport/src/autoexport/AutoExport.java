/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package autoexport;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;  
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Random;


/**
 *
 * @author gdunkley
 */
public class AutoExport {

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws FileNotFoundException, IOException, InterruptedException {
        
        /**
         * Note:
         * args "order" config.txt 
         * 
         * defining full path and variables from args[]
         */
               
        File propertiesFile = new File(args[0]);
        String absolPath = propertiesFile.getAbsolutePath();
        String filePath = absolPath.substring(0,absolPath.lastIndexOf(File.separator));
        
        /**
         * defining variables
         */

        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(args[0]));
        } 
        catch (IOException e) {
            System.out.println("Error - IOException - File not found...");
        }
      
        /**
         * defining Strings & Arrays
         */ 
        
        String model = properties.getProperty("model"); 
        String codeLevel = properties.getProperty("codeLvl");
        String suffix = new SimpleDateFormat("yyyyMMdd_hhmmss'.zip'").format(new Date());
        String subSysName = properties.getProperty("subSysName");
        
                
        /**
         * defining File paths
         */

        File subSysJLib = new File(filePath + "/jlib/" + codeLevel + "/lib");
        File jSanRmi = new File(subSysJLib + "/JSanRmiApiEx.jar:" 
                + subSysJLib + "/JSanRmiServerUx.jar");
        File destDir = new File(properties.getProperty("destination"));
        File logDir = new File(destDir + "/log");
        
        Random rand = new Random();        
        File workDir = new File(filePath + "/tmp" + rand.nextInt(99999));
        String exporTxt = workDir + "/" + subSysName + "_export.txt";
        File javaOutput = new File(workDir + "/" + subSysName);
        File zipFileName = new File(destDir + "/" + model + "/" + subSysName "/" + subSysName + "_" + suffix);

        /**
         * Creation of the command.txt file for export config.
         */

        List<String> dmdCommand = new ArrayList<>();
        dmdCommand.add(properties.getProperty("ip"));
        dmdCommand.add(properties.getProperty("gx00_dkscn"));
        dmdCommand.add(properties.getProperty("gx00_dkscn"));
        dmdCommand.add(properties.getProperty("uidPwd"));
        dmdCommand.add("show");
        dmdCommand.add(properties.getProperty("PhyPG"));
        dmdCommand.add(properties.getProperty("PhyLDEV"));
        dmdCommand.add(properties.getProperty("PhyProc"));
        dmdCommand.add(properties.getProperty("PhyExG"));
        dmdCommand.add(properties.getProperty("PhyExLDEV"));
        dmdCommand.add(properties.getProperty("PhyCMPK"));
        dmdCommand.add(properties.getProperty("PhyMPU"));
        dmdCommand.add(properties.getProperty("PhyMPPK"));
        dmdCommand.add(properties.getProperty("PG"));
        dmdCommand.add(properties.getProperty("Ldev"));
        dmdCommand.add(properties.getProperty("Port"));
        dmdCommand.add(properties.getProperty("MFPort"));
        dmdCommand.add(properties.getProperty("PortWWN"));
        dmdCommand.add(properties.getProperty("LU"));
        dmdCommand.add(properties.getProperty("PPCGWWN"));
        dmdCommand.add(properties.getProperty("RemoteCopy"));
        dmdCommand.add(properties.getProperty("RCLU"));
        dmdCommand.add(properties.getProperty("RCLdev"));
        dmdCommand.add(properties.getProperty("UR"));
        dmdCommand.add(properties.getProperty("URjnl"));
        dmdCommand.add(properties.getProperty("URLu"));
        dmdCommand.add(properties.getProperty("URLdev"));
        dmdCommand.add(properties.getProperty("LdevEachOfCu"));
        dmdCommand.add(properties.getProperty("rangeS"));
        dmdCommand.add(properties.getProperty("rangeL"));
        dmdCommand.add(properties.getProperty("rangeGx00"));
        dmdCommand.add("outpath " + '"' + javaOutput + '"');
        dmdCommand.add(properties.getProperty("compression"));
        dmdCommand.add("apply");
        
        /**
         * Creation of temp dir to run export in.
         */

        if (!workDir.exists()){
            workDir.mkdir(); 
            System.out.println("Directory: " + workDir + " has been created.");
        }
         
        /**
         * Creation of "correct java command" logic
         */
        
        ExportConfig obj1 = new ExportConfig();
        String cli = obj1.cliCmd(model, subSysJLib, jSanRmi, exporTxt, logDir);
        System.out.println("---COMMAND: " + cli);
        
        /**
         * Creation of -Dmd.command file
         */
        obj1.writeFile(dmdCommand, exporTxt);
        
        /**
         * Run export
         */
        obj1.runExe(cli);     //running the java command.
        
        /**
         * post process and cleanup
         * zip file and delete temp dir
         */
        
        System.out.println("---Extracting file names in: " + javaOutput.getCanonicalPath());
        System.out.println("---Creating zip file");
        ZipTypes obj2 = new ZipTypes();
        obj2.zipRecursive(javaOutput, zipFileName);
        System.out.println("---Done");
        System.out.println("-Initiating cleanup process");
        obj1.delDirAll(workDir);
        System.out.println("---Export Done");
    }
}
