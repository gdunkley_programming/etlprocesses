/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package splunkparse;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.StringJoiner;

/**
 *
 * @author gdunkley
 */

public class ORWF {
    private Scanner datFile;

    public void OpenFile(String fileName){
        try {
            datFile = new Scanner (new File(fileName));
        }
        catch (Exception e){
            System.out.println("File dosn't exist. - This error");
        }
    }
    
    
    public String[] ReadToArray(String[] arrayLineRead){
        int i =0;
        while(datFile.hasNext()){
            arrayLineRead[i++] = datFile.nextLine();
        }
        return arrayLineRead;
    }

    public String[] TextToCsvArray(String[] fileArray) {
        int i=0;
        while(datFile.hasNext()) {
            String line = datFile.nextLine();
            String[] split = line.split("\\s+");            
            StringJoiner joiner = new StringJoiner(",");
            
            for (String strVal: split)
                joiner.add(strVal);
            
            line = joiner.toString();
            line = line.startsWith(",") ? line.substring(1) : line;
            fileArray[i++] = line;            
        }
        return fileArray;
    }
    
    public void PrintArray(String[] arrayFile) {
        System.out.println();
        System.out.println("############ Array Output ############");
        System.out.println();
        
        for (String strVal : arrayFile) {
            if (strVal != null)
                System.out.println(strVal);
        }
    }
    
    
    public void WriteFile(String[] arrayLineWrite, String fileName) throws FileNotFoundException {
        
        java.io.File file = new java.io.File(fileName);
        java.io.PrintWriter output = new java.io.PrintWriter(file);

        for (String name : arrayLineWrite){
            if (name !=  null){
                output.println(name);
            }
        }

    output.close();
    }
           
    public void CloseF() {
        datFile.close();
    }

}
