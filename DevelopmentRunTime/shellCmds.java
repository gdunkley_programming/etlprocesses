/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package capacityextraction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author gdunkley
 */
public class shellCmds {
    
    //declare variables
    String command;
        
    public int shellCmds(String command) throws IOException{
        String line;
        Process cmdProcess = Runtime.getRuntime().exec(command);
                
        BufferedReader output = new BufferedReader(new InputStreamReader(cmdProcess.getInputStream()));
        while ((line = output.readLine()) != null){
            System.out.println(line);
            
        }
        
        BufferedReader outputErr = new BufferedReader(new InputStreamReader(cmdProcess.getErrorStream()));
        while ((line = outputErr.readLine()) != null){
            System.out.println(line);
        }
        
        int retValue = cmdProcess.exitValue();
        return retValue;
        

    }
    
}
