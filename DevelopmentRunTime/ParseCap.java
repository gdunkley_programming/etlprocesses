/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parsecap;



import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;


/**
 *
 * @author gdunkley
 */
public class ParseCap {

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
                
        // declare string arrays.
        String[] arrayFile1 = new String[20000000];
        String[] arrayFile2 = new String[20000000];
        String[] arrayFileFinal = new String[200000000];
                       
        // create new objects
        OpenReadWriteFile objOrwf1 = new OpenReadWriteFile();
        OpenReadWriteFile objOrwf2 = new OpenReadWriteFile();
        OpenReadWriteFile objOrwfFinal = new OpenReadWriteFile();
        
        String txtFile = "ldev_iops1_MDCVSP01_short.txt";
        String csvFile = "ldevBySubsyDec_53280_short.txt";
        String outputFile = "MDCVSP01_perfCap.csv";
        
        /**
         * File 1 open, read to array and print contents.
         */
        objOrwf1.OpenFile(txtFile);
        objOrwf1.ReadToArray(arrayFile1);
        
        /**
        * 
         // Print out statements for first array.
        System.out.println();
        System.out.println("##################FILE1#########################");
        for (String strVal : arrayFile1)
        {
            if (strVal != null)
                System.out.println(strVal);
        }
        */
        
        /**
         * File 2 open, read to array and print contents
         */
        
        objOrwf2.OpenFile(csvFile);
        objOrwf2.TextToCsvArray(arrayFile2);
        // write file to new csv text file, for future use.
        objOrwf2.WriteFile(arrayFile2, "parsedCSV_ldevBySubDec.txt");
        
        /**
         * Print out statements for second array
         * 
        System.out.println();
        System.out.println("###################FILE2########################");
        for (String strVal : arrayFile2)
        {
            if (strVal != null)
                System.out.println(strVal);
        }
        *
        */
        
        
        /**
         *          
        System.out.println();
        System.out.println("###################List2########################");
        System.out.println();
        
        for (String strVal : arrayFile2)
        {
            if (strVal != null)
            {
                List<String> arrayList2 = Arrays.asList(strVal.split(","));
                //            System.out.println(arrayList2.toString());
                if (!"0".equals(arrayList2.get(8)) && !"0".equals(arrayList2.get(11))
                        && arrayList2.get(12) !="0" && arrayList2.get(13) !="0")
                {
                    System.out.println(arrayList2.get(1) + "\t" + arrayList2.get(5)
                            + "\t" + arrayList2.get(8) + "\t" + arrayList2.get(11)
                            + "\t" + arrayList2.get(12) + "\t" + arrayList2.get(13));   
                }
            }
        }
        * 
        */
        
        System.out.println();
        System.out.println("###################New CSV########################");
        System.out.println();
        
        
        StringBuilder strBld1 = new StringBuilder();
        int i=0;
        
        for (String strValLine : arrayFile1)
        {
            if (strValLine != null)
            {
                
                
                List<String> arrayList1 = Arrays.asList(strValLine.split(","));
                String hexNum = arrayList1.get(1).replace(":", "");
                
                if (!"LDEV Number".equals(hexNum)
                        && !"0.0".equals(arrayList1.get(2))
                        && !"0.0".equals(arrayList1.get(3)))
                {
                    strBld1.append(strValLine).append(",");
                    
                    int ldev = Integer.parseInt(hexNum, 16);                    
                    int count = i %= 10;                    
                    if (count != 0) {
                        System.out.print("*");                        
                    }                        
                    i++;
                        
                    /**
                     * New loop for second file.... 
                     */

                    for (String strValLine2 : arrayFile2)
                    {
                        if (strValLine2 != null)
                        {
                            List<String> arrayList2 = Arrays.asList(strValLine2.split(","));
                            String decNum = arrayList2.get(1);
                            
                            if (!"LDEV#".equals(decNum))
                            {
                                int ldev2 = Integer.parseInt(arrayList2.get(1));
                                if (ldev == ldev2)
                                {
                                    strBld1.append(arrayList2.get(5))
                                        .append(",")
                                        .append(arrayList2.get(8))
                                        .append(",")
                                        .append(arrayList2.get(11))
                                        .append(",")
                                        .append(arrayList2.get(12))
                                        .append(",")
                                        .append(arrayList2.get(13))
                                        .append(",");
                                    
                                }
                            }
                        }
                    }
                    strBld1.append("\n");
                }
            }
        }
        arrayFileFinal[i++] = strBld1.toString();    
        
        System.out.println();
        for (String strVal : arrayFileFinal){
            if (strVal != null)
                System.out.println(strVal);
        }
        
        System.out.println();
        
        objOrwfFinal.WriteFile(arrayFileFinal, outputFile);
                
        
        objOrwf1.CloseF();
        objOrwf2.CloseF();


    }
}
        

 
