 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package etl_hds_gseries;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 *  
 * @author gdunkley
 */
    
public class ETLBinaryClass {
    
    public String shellSyntax(String cmdType, String serialNum) {
        
        String newshellSyntax = new String();
        
        switch (cmdType) {
            
            case "cmdtierdat":
                File raidcom1 = new File("/HORCM/usr/bin/raidcom get ldev -ldev_list mapped -key tier -s "
                        + serialNum + " | awk '{if (NR!=1) {print}}' | awk '{$1=$1};1' | awk '{print $1, $2, $6, $7, $9, $12, $13, $14, $15, $16}'");
                newshellSyntax = raidcom1.toString();
                //Format - Serial#, LDEV#, VOL_Cap(BLK), PID, VOL_Used(BLK), T#1(MB), T#2(MB), T#3(MB)
                break;
                
            case "cmdfrntdat": 
                File raidcom2 = new File("/HORCM/usr/bin/raidcom get ldev -ldev_list mapped -key frnt -s "
                        + serialNum + " | awk '{if (NR!=1) {print}}' | awk '{$1=$1};1' | awk '{split($10,a,\":\") split($11,b,\":\") split($12,c,\":\") split($13,d,\":\") split($14,e,\":\") split($15,f,\":\"); "
                        + " print $1, $2, a[3], b[3], c[3], d[3], e[3], f[3]}'");
                //Format - Serial#, LDEV#, GRPNAME1, GRPNAME2, GRPNAME3, GRPNAME4, GRPNAME5, GRPNAME6
                newshellSyntax = raidcom2.toString();
                break;
            
            case "cmdportdat":
                File raidcom3 = new File("/HORCM/usr/bin/raidcom ");
                newshellSyntax = raidcom3.toString();
                break;
            
            case "sqlIn1":
                File sqlcom1 = new File("mysql --user=systemCmds --password=systemCmdUpdate CBDB < sqlOutput1.txt");
                newshellSyntax = sqlcom1.toString();
                break;
            
            case "sqlIn2":
                File sqlcom2 = new File("mysql --user=systemCmds --password=systemCmdUpdate CBDB < sqlOutput2.txt");
                newshellSyntax = sqlcom2.toString();
                break;
            
            case "sqlIn3":
                File sqlcom3 = new File("mysql --user=systemCmds --password=systemCmdUpdate CBDB < sqlOutput3.txt");
                newshellSyntax = sqlcom3.toString();
                break;
            
            case "XXX":
                File XXXcom1 = new File("ifconfig");
                newshellSyntax = XXXcom1.toString();
                break;
        }
        return newshellSyntax;
    }
    
    
    public void runShellSyntax(String command, String directory){
        
        try {
            Process process = new ProcessBuilder(new String[] {"bash", "-c", command})
                    .redirectErrorStream(true)
                    .directory(new File(directory))
                    .start();
            
            //process.waitFor();

            if (0 != process.waitFor()){
                System.out.println("runShellSyntax Process still running...");
            }
        } catch (IOException | InterruptedException e) {
            //add in error handeling here.
            System.out.println("Error occured with runShellSyntax method: " + e);
            
        }
    }

    
    public ArrayList<String> runShellSyntaxToArray(String command, String directory){
        
        try {
            Process process = new ProcessBuilder(new String[] {"bash", "-c", command})
                    .redirectErrorStream(true)
                    .directory(new File(directory))
                    .start();
            ArrayList<String> output = new ArrayList<>();
            
            BufferedReader buffR = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = buffR.readLine()) != null){
                output.add(line);
            }
            if (0 != process.waitFor()){
                return null;
            }
            return output;
            
        } catch(IOException | InterruptedException e) {
            // need to add more in here for correct error reporting.
            System.out.println("Error occured with runShellSyntaxToArray method: " + e);
            return null;
        }
    }
    
    
    
    public String[] tierParse(ArrayList<String> inArrayData){
        
        //define date string
        LocalDateTime curDate = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String formDate = curDate.format(formatter);
        
        String[] arrayFile = new String[10000];
        
        /**
         * Insert statement for tier information.
         */
        
        StringBuilder strBld1 = new StringBuilder();
        String tableName1 = "Ldevs";
        int i=0;
        int lineCount=0;
        
        // Building statement for tier information sql command.
        strBld1.append("INSERT INTO ").append(tableName1)
                .append(" VALUES").append("\n");
        
        for (String strValLine: inArrayData){
            if (strValLine != null){
                if (lineCount++ != 0){
                    strBld1.append(", \n");
                }
                //create string list
                List<String> arrayList1 = Arrays.asList(strValLine.split(" "));
                
                strBld1.append("(")
                        .append(arrayList1.get(0)).append(", ")  
                        .append(arrayList1.get(1)).append(", ")
                        .append("'").append(formDate).append("', ")
                        .append(arrayList1.get(3)).append(", ")
                        .append(arrayList1.get(2)).append(", ")
                        .append(arrayList1.get(4)).append(", ")
                        .append(arrayList1.get(5)).append(", ")
                        .append(arrayList1.get(6)).append(", ")
                        .append(arrayList1.get(7)).append(")");
            }
            
            else {
                strBld1.append(";");    //this is almost obsolete and possibly should be taken out.
                break;
            }
            
        }
        //As inArrayData is a ArrayList no null should exist so ; needs to be added to the end.
        strBld1.append(";");
        arrayFile[i++] = strBld1.toString();
        
        return arrayFile;
    }
    
    
    public String[] frntParse(ArrayList<String> inArrayData){
        
        /**
         * Insert statement for frnt (front) information.
         */
        String[] arrayFile = new String[10000];
        
        StringBuilder strBld2 = new StringBuilder();
        String tableName2 = "Group_Name";
        int j=0;
        int lineCount=0;
        
        // Building statement for frnt information sql command.
        strBld2.append("INSERT INTO ").append(tableName2)
                .append(" VALUES").append("\n");
        
        for (String strValLine: inArrayData){
            if (strValLine != null){
                
                if (lineCount++ != 0){
                    strBld2.append(", \n");
                }
                //create string list
                
                String[] tempStr = new String[6];
                
                List<String> arrayList2 = Arrays.asList(strValLine.split(" ")); 
                  strBld2.append("(")
                          .append(arrayList2.get(0)).append(", ")
                          .append(arrayList2.get(1)).append(", ");
                
                int p = 0;
                for (int n=2; n<arrayList2.size(); n++)
                {
                    tempStr[p++] = arrayList2.get(n);
                }
                
                String[] uniqueVals = new HashSet<>(Arrays.asList(tempStr)).toArray(new String[0]);

                int count = 0;
                int countMax = 7;
                for (String strVal: uniqueVals){
                    if (strVal !=null){
                        strBld2.append("'").append(strVal).append("'");
                        count++;
                        if (count != countMax){
                            strBld2.append(", ");
                        }    
                    }
                }
                
                for (int i=count; i <= countMax; i++){
                    if (count < countMax){
                        strBld2.append("null, ");
                        count++;
                    }
                    else if (count == countMax){
                        strBld2.append("null )");
                    }
                }
                
            }
//            else {
//                strBld2.append(";");  //this is almost obsolete and possibly should be taken out.
//                break;
//            }

        }
        //As inArrayData is a ArrayList no null should exist so ; needs to be added to the end.
        strBld2.append(";");
        arrayFile[j++] = strBld2.toString();
        return arrayFile;
    }
}
