/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Psudocode
 * 
 * declare variables
 * reference/read properties or serial number file for multiple units
 * create raidcom commands
 * create loop for each unit/serial number
 *      run cmds and output to array per unit
 *      combine cmd outputs to one output based on keys
 *      1 - either output combined array to database per unit/serial number
 *      2 - OR output (append) combined array to file/Large Array/List 
 *          2.1 - output all units/serial numbers to database.
 * 
 */

package etl_hds_gseries;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author gdunkley
 */
public class ETL_HDS_GSeries {

   /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     * @throws java.io.IOException
     * @throws java.lang.InterruptedException
     */
    
    public static void main(String[] args) throws FileNotFoundException, IOException, InterruptedException {
        
        
        //Opening message
        System.out.println("\n\n\t\t Starting ETL Process...\n\n");
        //declare strings
        String txtFile1 = args[0];
        
        //declare Arrays
        ArrayList<String> arraytierdat;
        ArrayList<String> arrayfrntdat;
        String[] arrayFile1;
        String[] arrayFile2;
        String[] arrayFile3;
        String[] serialNum = new String[20];             

        //declare objects
        ETLBinaryClass objSBC1 = new ETLBinaryClass();
        ORWF objorwf1 = new ORWF();

        //opening serial file
        objorwf1.OpenFile(txtFile1);
        objorwf1.ReadToArray(serialNum);
        
        // define commands to run for input, then runing them.
        for (String strVal: serialNum){
            if (strVal != null){
                String cmd1 = objSBC1.shellSyntax("cmdtierdat", strVal);
                String cmd2 = objSBC1.shellSyntax("cmdfrntdat", strVal);
                String cmd3 = objSBC1.shellSyntax("sqlIn1", strVal);
                String cmd4 = objSBC1.shellSyntax("sqlIn2", strVal);
                                               
                //envoke commands to extrat data from units
                System.out.println("tier data now extracting for serial: " + strVal);
                arraytierdat = objSBC1.runShellSyntaxToArray(cmd1, ".");    
                System.out.println("frnt data now extracting for serial: " + strVal);
                arrayfrntdat = objSBC1.runShellSyntaxToArray(cmd2, ".");
                
                
                //run parse methods
                System.out.println("Parsing data...[tier]");
                arrayFile1 = objSBC1.tierParse(arraytierdat);
                objorwf1.WriteFile(arrayFile1, "sqlOutput1.txt");
                
                System.out.println("Parsing data...[frnt]");
                arrayFile2 = objSBC1.frntParse(arrayfrntdat);
                objorwf1.WriteFile(arrayFile2, "sqlOutput2.txt");               

                //run sql update commands
                System.out.println("Database update for tier data on: " + strVal);
                objSBC1.runShellSyntax(cmd3, ".");
                System.out.println("Database update for frnt data on: " + strVal);
                objSBC1.runShellSyntax(cmd4, ".");
                
                System.out.println("\n\n\t\t Finishing ETL Process for " + strVal + "\n\n");
                      
            }
        }  
        objorwf1.CloseF();
        System.out.println("\n\n\t\t Finishing ETL Process...\n\n");
        
    }    
}
